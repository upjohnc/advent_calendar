import operator
import re

# Part 1
ASSERT_TEXT_PART_1 = """b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"""

OPERATOR_METHOD = {'<': operator.lt, '>': operator.gt, '>=': operator.ge, '<=': operator.le, '!=': operator.ne, '==': operator.eq}


def parse_row(row_):
    groups = re.match(r'(\w+) (inc|dec) (-?\d+) if (\w+) ([<>=!]+) (-?\d+)', row_).groups()
    return groups


def inc_dec(action, register_value, action_value):
    if action.lower() == 'inc':
        return register_value + action_value
    else:
        return register_value - action_value


def run_part_1(input_text):
    rows = [i for i in input_text.split('\n') if len(i.strip()) > 0]

    registers = dict()

    def take_action(input_row):
        register_one = input_row[0]
        action = input_row[1]
        action_value = int(input_row[2])
        register_two = input_row[3]
        operator_ = input_row[4]
        operator_value = int(input_row[5])
        if register_one not in registers.keys():
            registers[register_one] = 0
        if register_two not in registers.keys():
            registers[register_two] = 0
        if OPERATOR_METHOD[operator_](registers[register_two], operator_value):
            registers[register_one] = inc_dec(action, registers[register_one], action_value)

    for row in rows:
        take_action(parse_row(row))
    return max(registers.values())


# Part 2
def run_part_2(input_text):
    rows = [i for i in input_text.split('\n') if len(i.strip()) > 0]

    registers = dict()
    max_register_value = 0

    def take_action(input_row, max_value):
        register_one = input_row[0]
        action = input_row[1]
        action_value = int(input_row[2])
        register_two = input_row[3]
        operator_ = input_row[4]
        operator_value = int(input_row[5])
        if register_one not in registers.keys():
            registers[register_one] = 0
        if register_two not in registers.keys():
            registers[register_two] = 0
        if OPERATOR_METHOD[operator_](registers[register_two], operator_value):
            registers[register_one] = inc_dec(action, registers[register_one], action_value)
            if registers[register_one] > max_value:
                max_value = registers[register_one]
        return max_value

    for row in rows:
        max_register_value = take_action(parse_row(row), max_register_value)
    return max_register_value


if __name__ == '__main__':
    run_part_1(ASSERT_TEXT_PART_1)
    # Part 1
    assert run_part_1(ASSERT_TEXT_PART_1) == 1
    with open('./day_08.txt', 'r') as f:
        the_text = f.read()
    print(run_part_1(the_text))

    # Part 2
    assert run_part_2(ASSERT_TEXT_PART_1) == 10
    print(run_part_2(the_text))
