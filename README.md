# Advent of Code
This is a solutions to the Advent in Code kata: [Advent in Code](https://adventofcode.com/2017/)

I used Tim Pote's solution as reference for improvements on my code. [Tim's Repo](https://github.com/potetm/advent-2017)

Cheat Sheet: [stolen example](https://chrxs.net/tag/advent-of-code/)
