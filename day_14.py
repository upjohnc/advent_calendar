from functools import reduce


def run_round(lengths, sparse_hash, pos=0, skip=0):
    max_len = len(sparse_hash)
    for length in lengths:
        pos %= max_len
        subset = list(
            reversed((sparse_hash + sparse_hash)[pos:(pos + length)]))
        for d in subset:
            pos %= max_len
            sparse_hash[pos] = d
            pos += 1
        pos += skip
        skip += 1
    return sparse_hash, pos % max_len, skip % max_len


def knot_hash(input_):
    lengths = []
    if len(input_) > 1:
        lengths += [ord(i) for i in input_]
    lengths += [17, 31, 73, 47, 23]
    pos = 0
    skip = 0
    sparse_hash = list(range(0, 256))
    for i in range(0, 64):
        sparse_hash, pos, skip = run_round(
            lengths, sparse_hash, pos=pos, skip=skip)

    dense_hash = ''
    for i in range(0, int(len(sparse_hash) / 16)):
        chunk = sparse_hash[(i * 16):(i * 16 + 16)]
        dense_hash += '%0.2X'.lower() % reduce(lambda j, k: j ^ k, chunk)

    return dense_hash


def run_part_1(input_string):
    used = 0
    for i in range(128):
        row = '{}-{}'.format(input_string, i)
        thing = knot_hash(row)
        binary = [bin(int(digit, 16))[2:].zfill(4) for digit in thing]
        used += len(''.join(binary).replace('0', ''))

    return used


assert run_part_1('flqrgnkx') == 8108
assert run_part_1('uugsqrei') == 8194


def fill_region(disk, row, col, region):
    disk[row][col] = region
    if col - 1 > -1 and disk[row][(col - 1)] == 1:
        fill_region(disk, row, col - 1, region)
    if col + 1 < len(disk[row]) and disk[row][(col + 1)] == 1:
        fill_region(disk, row, col + 1, region)
    if row - 1 > -1 and disk[(row - 1)][col] == 1:
        fill_region(disk, row - 1, col, region)
    if row + 1 < len(disk) and disk[(row + 1)][col] == 1:
        fill_region(disk, row + 1, col, region)


def run_part_2(input_):
    disk = []
    for i in range(0, 128):
        row = '{}-{}'.format(input_, i)
        binary = [bin(int(digit, 16))[2:].zfill(4) for digit in knot_hash(row)]
        disk.append([int(i) for i in list(''.join(binary))])

    region = 2
    for row in range(0, len(disk)):
        for col in range(0, len(disk[row])):
            if disk[row][col] == 1:
                fill_region(disk, row, col, region)
                region += 1
    return region - 2


print(run_part_2('uugsqrei'))
