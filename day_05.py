# Part 1
def command_calc(kwarg):
    index_of_instruction = kwarg['index_of_instruction']
    list_of_commands = kwarg['list_of_commands']
    # get instruction for list of commands
    instruction = list_of_commands[index_of_instruction]
    # get the next index based on the present index and the instruction
    new_index = instruction + index_of_instruction
    new_instruction_value = instruction + 1
    new_list_of_commands = list_of_commands.copy()
    new_list_of_commands[index_of_instruction] = new_instruction_value
    return {'new_index': new_index, 'new_list_of_commands': new_list_of_commands}


def run_part_1(list_of_commands):
    start_index = 0

    data_kwarg = {'index_of_instruction': start_index, 'list_of_commands': list_of_commands}
    result_list = [list_of_commands]
    while True:
        index_ = data_kwarg['index_of_instruction']
        latest_list_of_instructions = result_list[-1]
        value_at_index = latest_list_of_instructions[index_]
        if index_ + value_at_index + 1 > len(latest_list_of_instructions):
            return len(result_list)
        new_kwarg = command_calc(data_kwarg)
        data_kwarg = {'index_of_instruction': new_kwarg['new_index'], 'list_of_commands': new_kwarg['new_list_of_commands']}
        result_list.append(data_kwarg['list_of_commands'])


# Part 2
def command_calc_2(kwarg):
    index_of_instruction = kwarg['index_of_instruction']
    list_of_commands = kwarg['list_of_commands']
    # get instruction for list of commands
    instruction = list_of_commands[index_of_instruction]
    # get the next index based on the present index and the instruction
    new_index = instruction + index_of_instruction
    new_instruction_value = instruction + 1 if instruction < 3 else instruction - 1
    new_list_of_commands = list_of_commands.copy()
    new_list_of_commands[index_of_instruction] = new_instruction_value
    return {'new_index': new_index, 'new_list_of_commands': new_list_of_commands}


def run_part_2(list_of_commands):
    start_index = 0

    data_kwarg = {'index_of_instruction': start_index, 'list_of_commands': list_of_commands}
    steps_count = 1
    while True:
        index_ = data_kwarg['index_of_instruction']
        latest_list_of_instructions = data_kwarg['list_of_commands']
        value_at_index = latest_list_of_instructions[index_]
        if index_ + value_at_index + 1 > len(latest_list_of_instructions):
            return steps_count
        new_kwarg = command_calc_2(data_kwarg)
        data_kwarg = {'index_of_instruction': new_kwarg['new_index'], 'list_of_commands': new_kwarg['new_list_of_commands']}
        steps_count += 1


if __name__ == '__main__':
    # Part 1
    test_list = [0, 3, 0, 1, -3]
    assert run_part_1(test_list) == 5
    with open('./day_05_part1_input.txt', 'r') as f:
        puzzle_input_raw = f.read()
    puzzle_input = [int(i) for i in puzzle_input_raw.split('\n') if i != '']
    # print('Part 1: ', run_part_1(puzzle_input))

    # Part 2
    assert run_part_2(test_list) == 10
    with open('./day_05_part2_input.txt', 'r') as f:
        puzzle_input_raw = f.read()
    puzzle_input = [int(i) for i in puzzle_input_raw.split('\n') if i != '']
    print('Part 2: ', run_part_2(puzzle_input))
