import re
from functools import reduce


# Part 1
def strip_string(string_input):
    string_exclamation = re.sub(r'(!!)+', '', string_input)
    string_char_remove = re.sub(r'!.', '', string_exclamation)
    string_remove_garbage = re.sub(r'<>', '', re.sub(r'<[^>]+>', '', string_char_remove))
    string_parenthesis = re.sub(r'[^{}]', '', string_remove_garbage)
    return string_parenthesis


def sum_parenthesis(input_tuple, character):
    sum_scores = input_tuple[0]
    score = input_tuple[1]
    if character == '{':
        score += 1
        sum_scores += score
    else:
        score -= 1
    return sum_scores, score


def run_part_1(the_string):
    string_processed = strip_string(the_string)
    calc_value = reduce(sum_parenthesis, string_processed, (0, 0))
    sum_of_scores = calc_value[0]
    return sum_of_scores


# Part 2
def strip_string_part_2(string_input):
    string_exclamation = re.sub(r'(!!)+', '', string_input)
    string_char_remove = re.sub(r'!.', '', string_exclamation)
    return string_char_remove


def run_part_2(the_string):
    reduced_string = strip_string_part_2(the_string)
    what = re.findall(r'<([^>]+)>', reduced_string)
    return sum([len(i) for i in what])


if __name__ == '__main__':
    # Part 1
    # assert run_part_1('{}') == 1
    assert run_part_1('{{{}}}') == 6
    assert run_part_1('{{},{}}') == 5
    assert run_part_1('{{{},{},{{}}}}') == 16
    assert run_part_1('{<a>,<a>,<a>,<a>}') == 1
    assert run_part_1('{{<ab>},{<ab>},{<ab>},{<ab>}}') == 9
    assert run_part_1('{{<!!>},{<!!>},{<!!>},{<!!>}}') == 9
    assert run_part_1('{{<a!>},{<a!>},{<a!>},{<ab>}}') == 3
    with open('./day_09.txt', 'r') as f:
        raw_string = f.read()
    test_string = raw_string.strip()
    print(run_part_1(test_string))

    # Part 2
    assert run_part_2('<>') == 0
    assert run_part_2('<random characters>') == 17
    assert run_part_2('<<<<>') == 3
    assert run_part_2('<{!>}>') == 2
    assert run_part_2('<!!>') == 0
    assert run_part_2('<!!!>>') == 0
    assert run_part_2('<{o"i!a,<{i<a>') == 10
    with open('./day_09.txt', 'r') as f:
        raw_string = f.read()
    test_string = raw_string.strip()
    print(run_part_2(test_string))
