import re
from functools import reduce

import progressbar

thing = 's1,x3/4,pe/b, x15/20'

regex_parse = re.compile(r'(s)(\d+)|(x)(\d+)\/(\d+)|(p)(\w+)\/(\w+)')


def parse_input_string(input_string):
    parsed_groups = regex_parse.match(input_string.strip()).groups()
    parsed_simplified = list(filter(lambda x: x is not None, parsed_groups))
    if 's' in parsed_simplified:
        parsed_simplified[1] = int(parsed_simplified[1])
        parsed_simplified.append(None)
    if 'x' in parsed_simplified:
        parsed_simplified[1] = int(parsed_simplified[1])
        parsed_simplified[2] = int(parsed_simplified[2])
    parsed_tuple = tuple(parsed_simplified)

    return parsed_tuple


def convert_string_to_dict(the_string):
    def dict_thing(a, y):
        a[y[0]] = y[1]
        return a

    return reduce(dict_thing, enumerate(the_string), dict())


def s_dance(kwarg):
    program = kwarg['program']
    numeral = kwarg['numeral_input']

    new_program = program.copy()

    program_length = len(program.keys())

    for index_ in range(0, program_length - numeral):
        new_program[index_ + numeral] = program[index_]
    for count, index_ in enumerate(range(program_length - numeral, program_length)):
        new_program[count] = program[index_]

    return new_program


def x_dance(kwarg):
    program = kwarg['program']
    numeral_1 = kwarg['numeral_1_input']
    numeral_2 = kwarg['numeral_2_input']

    new_program = program.copy()
    new_program[numeral_1] = program[numeral_2]
    new_program[numeral_2] = program[numeral_1]

    return new_program


def p_dance(kwarg):
    program = kwarg['program']
    char_1 = kwarg['char_1_input']
    char_2 = kwarg['char_2_input']

    new_program = program.copy()

    def get_index(input_dict, char):
        for key, value in input_dict.items():
            if value == char:
                return key

    index_1 = get_index(program, char_1)
    index_2 = get_index(program, char_2)

    new_program[index_1] = char_2
    new_program[index_2] = char_1

    return new_program


assert s_dance({'program': convert_string_to_dict('abcde'), 'numeral_input': 3}) == {0: 'c', 1: 'd', 2: 'e', 3: 'a', 4: 'b'}
assert s_dance({'program': convert_string_to_dict('abcde'), 'numeral_input': 1}) == {0: 'e', 1: 'a', 2: 'b', 3: 'c', 4: 'd'}
s_dance_part = s_dance({'program': convert_string_to_dict('abcde'), 'numeral_input': 1})
assert x_dance({'program': s_dance_part, 'numeral_1_input': 3, 'numeral_2_input': 4}) == {0: 'e', 1: 'a', 2: 'b', 3: 'd', 4: 'c'}
x_dance_part = x_dance({'program': s_dance_part, 'numeral_1_input': 3, 'numeral_2_input': 4})
assert p_dance({'program': x_dance_part, 'char_1_input': 'e', 'char_2_input': 'b'}) == {0: 'b', 1: 'a', 2: 'e', 3: 'd', 4: 'c'}


def create_original_program():
    a_char = 97

    def dict_thing(a, y):
        a[y[0]] = y[1]
        return a

    return reduce(dict_thing, enumerate(map(lambda x: str(chr(x)), range(a_char, a_char + 16))), dict())


with open('./day_16_input.txt', 'r') as f:
    input_ = f.read()

original_state = the_program = create_original_program()


class Memoize:
    def __init__(self, f):
        self.f = f
        self.memo = dict()

    def __call__(self, *args):
        if str(args[0]) not in self.memo.keys():
            self.memo[str(args[0])] = self.f(args[0])
        return self.memo[str(args[0])]


# Part 1
@Memoize
def run_part_1(input_program):
    for instructions in tuple(map(parse_input_string, input_.split(','))):
        if 's' in instructions[0]:
            input_program = s_dance({'program': input_program, 'numeral_input': instructions[1]})
        elif 'x' in instructions[0]:
            input_program = x_dance({'program': input_program, 'numeral_1_input': instructions[1], 'numeral_2_input': instructions[2]})
        elif 'p' in instructions[0]:
            input_program = p_dance({'program': input_program, 'char_1_input': instructions[1], 'char_2_input': instructions[2]})
        else:
            print('error in parsing')
    return input_program


def convert_back_to_string(the_dict):
    return ''.join(the_dict.values())


assert convert_back_to_string(run_part_1(the_program)) == 'fnloekigdmpajchb'

# find the cycle that returns to the original state (note: index on 0, so need to add one to output)
the_program = original_state
for cycle in range(1000000000):
    the_program = run_part_1(the_program)
    if the_program == original_state:
        print('cycle: ', cycle + 1)
        break

# after 60 cycles the string is back to its original state
# 1e9 % 60 = 40
# just need to check for the end of 40 cycles
bar = progressbar.ProgressBar()
the_program = original_state
for q in bar(range(40)):
    the_program = run_part_1(the_program)

assert convert_back_to_string(the_program) == 'amkjepdhifolgncb'
