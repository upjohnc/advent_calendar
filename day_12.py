import re
from functools import reduce


# Part 1
def parse_it(input_):
    string_check = re.compile(r'^\s*(\d+)\s*<->\s*([\d,\s]+)')

    def parse_row(row_):
        grouped_string = string_check.match(row_).groups()
        response = {'parent': grouped_string[0], 'children': grouped_string[1]}
        return response

    return {q['parent']: q['children'] for q in map(parse_row, input_)}


def thing(input_, parent, children, checked):
    checked.append(parent)
    for child in input_[parent].split(','):
        child_strip = child.strip()
        if child_strip not in checked:
            children.append(child_strip)
            thing(input_, child_strip, children, checked)
    return children


def parent_child(more_input):
    hmm = parse_it(more_input)
    end_thing = dict()
    for i in hmm.keys():
        result = thing(hmm, i, list(), list())
        end_thing[i] = result
    return end_thing


def run_part_1(input_yes):
    return len(parent_child(input_yes)['0']) + 1


def run_part_2(input_yes):
    so_much_more = parent_child(input_yes)
    checker = [list(sorted(v + [k])) for k, v in so_much_more.items()]

    def append_new_items(a, x):
        if x not in a:
            a.append(x)
        return a

    end_of_all = reduce(append_new_items, checker, list())
    return len(end_of_all)


if __name__ == '__main__':
    the_input = """0 <-> 2
        1 <-> 1
        2 <-> 0, 3, 4
        3 <-> 2, 4
        4 <-> 2, 3, 6
        5 <-> 6
        6 <-> 4, 5
        """
    good_input = list(filter(lambda x: len(x.strip()) > 0, the_input.split('\n')))
    what_is = parent_child(good_input)
    assert what_is == {
        '0': ['2', '3', '4', '6', '5'],
        '1': [],
        '2': ['0', '3', '4', '6', '5'],
        '3': ['2', '0', '4', '6', '5'],
        '4': ['2', '0', '3', '6', '5'],
        '5': ['6', '4', '2', '0', '3'],
        '6': ['4', '2', '0', '3', '5']
    }

    # Part 1
    assert run_part_1(good_input) == 6

    with open('./day_12_input.txt', 'r') as f:
        input_raw = f.read()
    input_clean = list(filter(lambda x: len(x.strip()) > 0, input_raw.split('\n')))
    assert run_part_1(input_clean) == 115

    # Part 2
    run_part_2(good_input)
    assert run_part_2(input_clean) == 221
