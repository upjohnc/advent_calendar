# calculation is the the distance from the cell to the center points of the level plus the value of the level.


def round_up(value):
    return int(value) if value % 1 == 0 else int(value) + 1


def find_level(cell_number):
    """get the level of the cell.  the center cell (1) is considered the zeroth level."""
    level_calculated = (cell_number**0.5 - 1) / 2
    return round_up(level_calculated)


def get_axis_of_level(level_value):
    """calculate the axis points (the center points between the corners)"""
    base_formula = (2 * level_value + 1)**2
    bottom = base_formula - level_value
    left = base_formula - 3 * level_value
    top = base_formula - 5 * level_value
    right = base_formula - 7 * level_value
    return top, left, bottom, right


def calc_distance_from_axis(kwarg):
    """calculate the distance from the nearest axis point"""
    level = kwarg['level']
    cell_number = kwarg['cell_number']
    axes = get_axis_of_level(level)
    min_distance = min([abs(cell_number - i) for i in axes])
    return min_distance


def full_calc(kwarg):
    """calculate the number of steps to the center cell from the given cell"""
    cell = kwarg['cell']
    level = find_level(cell)
    return calc_distance_from_axis({'level': level, 'cell_number': cell}) + level


# Part 2


def tuple_addition(tuple_1, tuple_2):
    return tuple_1[0] + tuple_2[0], tuple_1[1] + tuple_2[1]


def calc_cell_value(kwarg):
    cell_values_dict = kwarg['cell_values']
    cell_tuple = kwarg['cell_tuple_position']
    neighbors = kwarg['neighbors']

    new_cell_value = sum([cell_values_dict.get(tuple_addition(cell_tuple, i), 0) for i in neighbors])
    return new_cell_value


def day3_part2(given_cell):
    """the next cell is in one direction for a particular count - pattern [1, 1, 2, 2, 3, 4]
    it will move that number of steps and then turn counter-clockwise.
    it will turn once and then a next pattern is done.
    """
    first_cell_value = 1
    cell_values = {(0, 0): first_cell_value}
    neighbors = [(0, 1), (0, -1), (1, 0), (-1, 0), (-1, -1), (1, -1), (1, 1), (-1, 1)]
    movement_pattern = ('r', 'u', 'l', 'd')
    movement_tuple = {'r': (0, 1), 'u': (1, 0), 'l': (0, -1), 'd': (-1, 0)}
    cell_tuple_start = (0, 0)
    movement_turns_max = 1
    movement_direction_count_max_start = 1
    start_cell_sum = 1
    movement_direction_start = 0
    state = {
        'turns': 0,
        'movement_direction_index': movement_direction_start,
        'movement_direction_count_max': movement_direction_count_max_start,
        'movement_direction_count': 0,
        'present_cell_sum': start_cell_sum,
        'present_cell_tuple': cell_tuple_start
    }
    while True:
        present_cell_sum = state['present_cell_sum']
        # run to get the next sum_value after the given_cell
        if given_cell >= present_cell_sum:
            movement_tuple_next = movement_tuple[movement_pattern[state['movement_direction_index']]]
            present_cell_tuple_position = tuple_addition(movement_tuple_next, state['present_cell_tuple'])
            state['present_cell_tuple'] = present_cell_tuple_position
            present_cell_sum = calc_cell_value({
                'cell_values': cell_values,
                'cell_tuple_position': present_cell_tuple_position,
                'neighbors': neighbors
            })
            cell_values[present_cell_tuple_position] = present_cell_sum
            state['present_cell_sum'] = present_cell_sum
            state['movement_direction_count'] += 1
            if state['movement_direction_count'] >= state['movement_direction_count_max']:
                state['turns'] += 1
                if state['turns'] > movement_turns_max:
                    state['turns'] = 0
                    state['movement_direction_count_max'] += 1
                state['movement_direction_count'] = 0
                state['movement_direction_index'] = (state['movement_direction_index'] + 1) % 4
        else:
            return present_cell_sum


if __name__ == '__main__':
    # part 1
    assert full_calc({'cell': 1}) == 0
    assert full_calc({'cell': 12}) == 3
    assert full_calc({'cell': 23}) == 2
    assert full_calc({'cell': 1024}) == 31
    print(full_calc({'cell': 265149}))

    # part 2
    assert day3_part2(57) == 59
    assert day3_part2(142) == 147
    assert day3_part2(747) == 806
    print(day3_part2(265149))
