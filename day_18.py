from pyrsistent import freeze
from collections import defaultdict


def set_(kwarg):
    return kwarg['action_value']


def add(kwarg):
    return kwarg['register'] + kwarg['action_value']


def mul(kwarg):
    param_2 = kwarg['action_value']
    param_1 = kwarg['register']
    return param_1 * param_2


def mod(kwarg):
    return kwarg['register'] % kwarg['action_value']


commands_dict = {'set': set_, 'add': add, 'mul': mul, 'mod': mod}


def set_data(input_):
    def thing(row):
        parse_row = row.split()
        try:
            parse_row[2] = int(parse_row[2])
        except Exception as e:
            pass
        return tuple(parse_row)

    return freeze({step: thing(the_row) for step, the_row in enumerate(input_.split('\n')) if len(the_row.strip()) != 0})


with open('./day_18_assert.txt', 'r') as f:
    assert_input = f.read()

assert_input = set_data(assert_input)


def parse_instructions(input_instructions):
    global_last_sound_value = 0
    global_register_value = defaultdict(int)

    instruction_index = 0

    while True:
        instruction = input_instructions[instruction_index]
        command = instruction[0]
        register = instruction[1]
        if command == 'rcv':
            if global_register_value[register] > 0:
                global_register_value[register] = global_last_sound_value
                break
            instruction_index += 1
        elif command == 'jgz':
            present_register_value = global_register_value[register]
            if present_register_value > 0:
                action_value = instruction[2]
                instruction_index = instruction_index + action_value
            else:
                instruction_index += 1
        elif command == 'snd':
            global_last_sound_value = global_register_value[register]
            instruction_index += 1
        else:
            action_value = instruction[2]
            if isinstance(action_value, str):
                action_value = global_register_value[action_value]
            global_register_value[register] = commands_dict[command]({'register': global_register_value[register], 'action_value': action_value})
            instruction_index += 1

        if instruction_index > max(input_instructions.keys()):
            break
    return global_last_sound_value


assert parse_instructions(assert_input) == 4

with open('./day_18_input.txt', 'r') as f:
    instructions_raw = f.read()

instructions_input = set_data(instructions_raw)

# Part 1
assert parse_instructions(instructions_input) == 7071
