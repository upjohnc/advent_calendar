from functools import reduce

# Part 1
# (x, y, z)
DIRECTIONS = {'n': (0, 1, -1), 's': (0, -1, 1), 'ne': (1, 0, -1), 'nw': (-1, 1, 0), 'se': (1, -1, 0), 'sw': (-1, 0, 1)}

KEYS = ('x', 'y', 'z')
CENTER = {k: 0 for k in KEYS}


def calc_move(one, two):
    return {k: one[k] + two[k] for k in KEYS}


def distance_calc(a, b):
    thing = [abs(a[k] - b[k]) for k in KEYS]
    return max(thing)


def set_dict(input_thing):
    return {'x': input_thing[0], 'y': input_thing[1], 'z': input_thing[2]}


def run_part_1(input_):
    move_input = list(map(set_dict, [DIRECTIONS[i] for i in input_]))
    end_cell = reduce(calc_move, move_input, {k: 0 for k in KEYS})
    return distance_calc(end_cell, CENTER)


def run_part_2(input_):
    # run through all the steps to retrieve the distance from the center for each of the steps
    thing = [run_part_1(input_[:i]) for i in range(len(input_))]
    # return the max of the distances
    return max(thing)


if __name__ == '__main__':
    # Part 1
    assert run_part_1(('ne', 'ne', 'ne')) == 3
    assert run_part_1(('ne', 'ne', 'sw', 'sw')) == 0
    assert run_part_1(('ne', 'ne', 's', 's')) == 2
    assert run_part_1(('se', 'sw', 'se', 'sw', 'sw')) == 3
    with open('day_11_input.txt', 'r') as f:
        input_data = f.read()

    print(run_part_1(input_data.replace('\n', '').split(',')))

    print(run_part_2(input_data.replace('\n', '').split(',')))
