# stolen day18 part 2

duet = [line.rstrip() for line in open('day_18_input.txt').readlines()]

registers = {0: {'p': 0}, 1: {'p': 1}}
queue = {0: [], 1: []}
state = {0: 'running', 1: 'running'}
sends = {0: 0, 1: 0}
i = {0: 0, 1: 0}
while True:
    cmds = {0: duet[i[0]][0:3], 1: duet[i[1]][0:3]}
    x = {0: duet[i[0]][4:5], 1: duet[i[1]][4:5]}
    y = {0: '', 1: ''}
    if len(duet[i[0]]) > 5:
        y[0] = duet[i[0]][6:len(duet[i[0]])]
    if len(duet[i[1]]) > 5:
        y[1] = duet[i[1]][6:len(duet[i[1]])]

    if state[0] == 'receiving' and state[1] == 'receiving':
        print('Deadlocked')
        print(sends)
        exit(0)

    for j, cmd in cmds.items():
        if x[j].isalpha() and x[j] not in registers[j]:
            registers[j][x[j]] = 0

        if cmd == 'snd':
            if j == 0:
                queue[1].insert(0, registers[j][x[j]])
            else:
                queue[0].insert(0, registers[j][x[j]])
            sends[j] += 1
        elif cmd == 'set':
            if y[j].isalpha():
                registers[j][x[j]] = registers[j][y[j]]
            else:
                registers[j][x[j]] = int(y[j])
        elif cmd == 'add':
            if y[j].isalpha():
                registers[j][x[j]] += registers[j][y[j]]
            else:
                registers[j][x[j]] += int(y[j])
        elif cmd == 'mul':
            if y[j].isalpha():
                registers[j][x[j]] *= registers[j][y[j]]
            else:
                registers[j][x[j]] *= int(y[j])
        elif cmd == 'mod':
            if y[j].isalpha():
                registers[j][x[j]] %= registers[j][y[j]]
            else:
                registers[j][x[j]] %= int(y[j])
        elif cmd == 'rcv':
            state[j] = 'receiving'
            if len(queue[j]) > 0:
                registers[j][x[j]] = queue[j].pop()
                state[j] = 'running'
        elif cmd == 'jgz':
            if (x[j].isalpha() and registers[j][x[j]] > 0) or (
                    x[j].isdigit() and int(x[j]) > 0):
                if y[j].isalpha():
                    i[j] += registers[j][y[j]] - 1
                else:
                    i[j] += int(y[j]) - 1

        if state[j] == 'running':
            i[j] += 1