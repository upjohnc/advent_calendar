# stole this one

import sys
from collections import deque
from itertools import cycle, islice

from progressbar import ProgressBar

bar = ProgressBar()
idx = 0
buffer = [0]
steps = int(sys.argv[1])
for i in bar(range(1, 2018)):
    spinner = cycle(buffer)
    spinner = islice(spinner, idx + 2, None)
    for j in range(0, steps):
        idx = buffer.index(next(spinner))
    buffer.insert(idx + 1, i)
print(buffer[buffer.index(2017) + 1])

# part 2

bar = ProgressBar()
spinner = deque([0])
for i in bar(range(1, 50000001)):
    spinner.rotate(-steps)
    spinner.append(i)

print(spinner[spinner.index(0) + 1])
