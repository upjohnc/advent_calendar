import copy


# Part 1
def calc_new_bank(kwarg):
    bank = kwarg['bank']
    # get largest value in bank
    largest_block = max(bank)

    # get index of first largest value in bank
    index_largest = bank.index(largest_block)

    # make value 0 at the largest value index
    new_bank = bank
    new_bank[index_largest] = 0

    # distribute into the blocks
    additional_blocks_for_all = largest_block // len(bank)
    new_bank = [i + additional_blocks_for_all for i in new_bank]
    left_over_blocks = largest_block % len(bank)
    left_over_indexes = [(index_largest + 1 + j) % len(bank) for j in range(left_over_blocks)]
    for index in left_over_indexes:
        new_bank[index] = new_bank[index] + 1

    return new_bank


def run_part_1(start_bank):
    present_bank = copy.copy(start_bank)
    list_of_banks = list()
    list_of_banks.append(start_bank)

    while True:
        present_bank = calc_new_bank({'bank': copy.copy(present_bank)})
        if present_bank in list_of_banks:
            return len(list_of_banks)
        list_of_banks.append(present_bank)


# Part 2
def run_part_2(start_bank):
    present_bank = copy.copy(start_bank)
    list_of_banks = list()
    list_of_banks.append(start_bank)

    while True:
        present_bank = calc_new_bank({'bank': copy.copy(present_bank)})
        if present_bank in list_of_banks:
            existing_index = list_of_banks.index(present_bank)
            return len(list_of_banks) - existing_index
        list_of_banks.append(present_bank)


if __name__ == '__main__':
    # part 1
    assert 5 == run_part_1([0, 2, 7, 0])
    input_list = [int(i) for i in '4	1	15	12	0	9	9	5	5	8	7	3	14	5	12	3'.split()]
    print('Part 1: ', run_part_1(input_list))
    # part 2
    assert 4 == run_part_2([0, 2, 7, 0])
    print('Part 2: ', run_part_2(input_list))
