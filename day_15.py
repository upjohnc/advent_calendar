from functools import reduce

DIVISOR = 2147483647
SEED_A = 65
SEED_B = 8921


def generator_a(prev_value):
    factor = 16807
    return (prev_value * factor) % DIVISOR


def generator_b(prev_value):
    factor = 48271
    return (prev_value * factor) % DIVISOR


def find_nth_value(seed_value, nth, generator_func):
    if nth < 1:
        return None
    while True:
        seed_value = generator_func(seed_value)
        if nth == 1:
            return seed_value
        nth -= 1


def get_binary(int_value):
    return bin(int_value)[-16:]


assert generator_a(SEED_A) == 1092455
assert find_nth_value(SEED_A, 1, generator_a) == 1092455
assert find_nth_value(SEED_A, 5, generator_a) == 1352636452

assert find_nth_value(SEED_B, 1, generator_b) == 430625591
assert find_nth_value(SEED_B, 3, generator_b) == 1431495498
assert find_nth_value(SEED_B, 5, generator_b) == 285222916


# Part 1
# def func_gen_a(a, x):
#     our_value = generator_a(a[-1])
#     a.append(our_value)
#     return a
#
#
# def func_gen_b(a, x):
#     our_value = generator_b(a[-1])
#     a.append(our_value)
#     return a
#
#
# def get_base_10_thing(function_, end_value, seed_value):
#     return reduce(function_, range(1, end_value + 1), [seed_value])
#
#
# total_count = int(4e7)
# binary_thing_a = list(map(get_binary, get_base_10_thing(func_gen_a, total_count, 873)))
# binary_thing_b = list(map(get_binary, get_base_10_thing(func_gen_b, total_count, 583)))
#
#
# def get_end_value(thing_a, thing_b):
#     zip_thing = zip(thing_a[1:], thing_b[1:])
#     return sum(map(lambda x: x[0] == x[1], zip_thing))
#
#
# assert get_end_value(binary_thing_a, binary_thing_b) == 631

# Part 2
total_count = int(5e6)


def run_part_2(comparison_count, seed_a, seed_b):
    comparisons = list()
    divisible_a = 4
    divisible_b = 8

    def inner_func():
        value_a = seed_a
        value_b = seed_b
        while True:
            if len(comparisons) > comparison_count:
                return comparisons
            while True:
                value_a = generator_a(value_a)
                if value_a % divisible_a == 0:
                    break
            while True:
                value_b = generator_b(value_b)
                if value_b % divisible_b == 0:
                    break
            comparisons.append(get_binary(value_a) == get_binary(value_b))

    return sum(inner_func())


# assert run_part_2(total_count, SEED_A, SEED_B) == 309

assert run_part_2(total_count, 873, 583) == 279
