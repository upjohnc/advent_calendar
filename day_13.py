from functools import reduce


def scanner_position(kwarg):
    time = kwarg['time']
    depth = kwarg['depth']
    d_thing = depth - 1
    mod_value = (depth - 1) * 2
    mod_result = time % mod_value
    if mod_result > d_thing:
        return (2 * d_thing) - mod_result
    else:
        return mod_result


def calc_severity(kwarg):
    level = kwarg['level']
    depth = kwarg['depth']
    return level * depth


def run_part_1(kwarg):
    grid = kwarg['grid']

    max_level = max(grid.keys())
    level_caught = list()
    for time in range(max_level + 1):
        if time in grid.keys():
            position = scanner_position({'time': time, 'depth': grid[time]})
            if position == 0:
                level_caught.append(time)

    def calc_sum_severity(a, x):
        a += calc_severity({'level': x, 'depth': grid[x]})
        return a

    sum_severity = reduce(calc_sum_severity, level_caught, 0)

    return sum_severity


def run_part_2(kwarg):
    grid = kwarg['grid']

    max_level = max(grid.keys())

    def get_depth_positions(time_offset):
        # get depth position of each time
        def inner(a, level):
            if level in grid.keys():
                position = scanner_position({'time': level + time_offset, 'depth': grid[level]})
                a.append(position)
            return a

        positions = reduce(inner, range(max_level + 1), list())
        return positions

    offset_check = 0
    # loop until find one that has success
    while True:
        # check if any of the times had 0 in it - return bad if there is 0
        thing = get_depth_positions(offset_check)
        if 0 in thing:
            offset_check += 1
        else:
            break

    return offset_check


if __name__ == '__main__':
    test_scanner_position_list = [((0, 3), 0), ((2, 3), 2), ((4, 3), 0), ((5, 3), 1), ((6, 3), 2), ((2, 2), 0), ((4, 2), 0), ((2, 4), 2),
                                  ((4, 4), 2), ((5, 4), 1), ((6, 4), 0), ((10, 6), 0), ((16, 6), 4)]
    for input_, result in test_scanner_position_list:
        assert scanner_position({'time': input_[0], 'depth': input_[1]}) == result

    test_severity_list = [((0, 3), 0), ((6, 4), 24)]
    for input_, result in test_severity_list:
        assert calc_severity({'level': input_[0], 'depth': input_[1]}) == result

    test_part_1 = {0: 3, 1: 2, 4: 4, 6: 4}
    assert run_part_1({'grid': test_part_1}) == 24

    with open('./day_13_input.txt', 'r') as f:
        input_raw = f.read()
    input_rows = list(filter(lambda x: len(x.strip()) > 0, input_raw.split('\n')))
    part_1_input = {int(i.split(':')[0]): int(i.split(':')[1]) for i in input_rows}

    assert run_part_1({'grid': part_1_input}) == 1640

    # Part 2
    assert run_part_2({'grid': test_part_1}) == 10
    assert run_part_2({'grid': part_1_input}) == 3960702
