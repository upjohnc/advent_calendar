def subtract_row(row):
    items = [int(i) for i in row.split()]
    return max(items) - min(items)


def sum_rows(calced_rows):
    return sum(calced_rows)


def run_checksum(spreadsheet):
    return sum_rows(map(subtract_row, spreadsheet.split('\n')))


def test_checksum():
    test_data = '5 1 9 5\n7 5 3\n2 4 6 8'
    assert run_checksum(test_data) == 18


def division_row(row):
    items = [int(i) for i in row.split()]

    for i in range(len(items)):
        first_element = items[i]
        for j in range((i + 1), len(items)):
            second_element = items[j]
            mod_result = first_element % second_element if first_element > second_element else second_element % first_element
            if mod_result == 0:
                return first_element / second_element if first_element > second_element else second_element / first_element


def run_divisionsum(spreadsheet):
    return sum_rows(map(division_row, spreadsheet.split('\n')))


def test_divisionsum():
    test_data = '5 9 2 8\n9 4 7 3\n3 8 6 5'
    assert run_divisionsum(test_data) == 9


if __name__ == '__main__':
    with open('day_02_temp.txt', 'r') as f:
        thing = f.read()

    print(run_checksum(thing))

    print(run_divisionsum(thing))
