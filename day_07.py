import re


# Part 1
def parse_text(text_raw):
    # added if statement for the last row is blank
    rows_arrow_split = [i.split('->') for i in text_raw.split('\n') if len(i.strip()) > 0]

    def build_dict(item_):
        first_part = item_[0]

        new_dict = {'weight': first_part.split()[1].strip()}
        if len(item_) > 1:
            new_dict['prog_above'] = [w.strip() for w in item_[1].split(',')]
        return {first_part.split()[0]: new_dict}

    return list(map(build_dict, rows_arrow_split))


def run_part_1(text_raw):
    thing = parse_text(text_raw)
    big_dict = dict()
    for w in thing:
        for key, value in w.items():
            if key not in big_dict.keys():
                big_dict[key] = 'bottom'
            if 'prog_above' in value.keys():
                for t in value['prog_above']:
                    big_dict[t] = 'not_bottom'
            else:
                big_dict[key] = 'not_bottom'
    result = [k for k, i in big_dict.items() if i == 'bottom']

    # affirm there is only one program that is at the bottom
    assert len(result) == 1, 'more than one item with "bottom" - error'

    return result[0]


# Part 2
def parse_text_part_2(text_raw):
    # added if statement for the last row is blank
    rows_ = [i for i in text_raw.split('\n') if len(i.strip()) > 0]

    def parse_with_regex(text_):
        the_match = re.match('(\w+) \((\d+)\)( -> ((\w+, )*\w+))?', text_)
        the_groups = the_match.groups()
        sub_programs = the_groups[3].split(',') if the_groups[3] is not None else None
        sub_programs = [i.strip() for i in sub_programs] if sub_programs is not None else None
        return the_groups[0], int(the_groups[1]), sub_programs

    separated_text = list(map(parse_with_regex, rows_))

    parsed_text = dict()
    for item in separated_text:
        parsed_text[item[0]] = {'weight': item[1]}
        if item[2] is not None:
            parsed_text[item[0]]['prog_above'] = item[2]

    return parsed_text


def get_subprograms(kwarg):
    full_text_parsed = kwarg['text_parsed']
    node_program = kwarg['node_program']

    check_for_subprograms = full_text_parsed[node_program].get('prog_above', None)
    if check_for_subprograms is not None:
        existing_subprograms = check_for_subprograms.copy()
    else:
        existing_subprograms = None

    while check_for_subprograms is not None and len(check_for_subprograms) > 0:
        next_program_check = check_for_subprograms[0]
        check_for_subprograms = check_for_subprograms[1:]
        new_subgprograms = full_text_parsed[next_program_check].get('prog_above', None)
        if new_subgprograms is not None:
            existing_subprograms.extend(new_subgprograms)
            check_for_subprograms.extend(new_subgprograms)
    return existing_subprograms


def get_node_weight(kwarg):
    parsed_text = kwarg['parsed_text']
    node_program = kwarg['primary_program']
    my_dict = dict()
    for sub_program in parsed_text[node_program]['prog_above']:
        new_programs = get_subprograms({'text_parsed': parsed_text, 'node_program': sub_program})
        node_weight = parsed_text[sub_program]['weight']
        total_weight = node_weight
        if new_programs is not None:
            weights = {m: parsed_text[m]['weight'] for m in new_programs}
            total_weight += sum([item for k, item in weights.items()])
        my_dict[sub_program] = {'sums': total_weight}

    return {'node_weights': my_dict, 'parsed_text': parsed_text}


def run_part_2(kwarg):
    test_node = kwarg['primary_program']
    text_raw = kwarg['text_raw']
    parsed_text_ = parse_text_part_2(text_raw)

    def get_unbalanced(kwarg_inner):
        test_result = get_node_weight({'parsed_text': parsed_text_, 'primary_program': kwarg_inner['primary_program']})

        node_weights = test_result['node_weights']
        if len(node_weights) == 0:
            return None
        average_weight = sum([i['sums'] for i in node_weights.values()]) / len(node_weights.keys())
        above_average = [key for key, items in node_weights.items() if items['sums'] > average_weight]
        below_average = [key for key, items in node_weights.items() if items['sums'] < average_weight]
        difference = None
        if len(above_average) != 0:
            difference = node_weights[above_average[0]]['sums'] - node_weights[below_average[0]]['sums']
        return {'above_average': above_average, 'below_average': below_average, 'difference': difference}

    test_node_difference = 0
    while True:
        unbalanced_result = get_unbalanced({'primary_program': test_node})
        if unbalanced_result is None:
            break
        if len(unbalanced_result['above_average']) == 1:
            test_node = unbalanced_result['above_average'][0]
            test_node_difference = unbalanced_result['difference']
        elif len(unbalanced_result['below_average']) == 1:
            test_node = unbalanced_result['below_average'][0]
            test_node_difference = unbalanced_result['difference']
        else:
            break

    return parsed_text_[test_node]['weight'] - test_node_difference


if __name__ == '__main__':
    # part 1
    with open('./day_07_assert_part_1.txt', 'r') as f:
        assert_text = f.read()

    assert run_part_1(assert_text) == 'tknk'
    with open('./day_07_part_1.txt', 'r') as f:
        part1_raw_text = f.read()
    print(run_part_1(part1_raw_text))

    # part 2
    core_node = run_part_1(assert_text)
    assert run_part_2({'text_raw': assert_text, 'primary_program': core_node}) == 60

    with open('./day_07_part_2.txt', 'r') as f:
        part2_raw_text = f.read()
    core_node = run_part_1(part2_raw_text)
    print(run_part_2({'text_raw': part2_raw_text, 'primary_program': core_node}))
