def reverse_list(the_list, present_index, reverse_length):
    list_index_split = present_index + reverse_length

    if list_index_split >= len(the_list):
        list_to_reverse = the_list[present_index:]
        list_to_reverse += the_list[0:list_index_split % len(the_list)]
    else:
        list_to_reverse = the_list[present_index:list_index_split]
    temp_list = list_to_reverse.copy()
    list_reversed = temp_list[::-1]
    return list_reversed


def reset_original(kwarg):
    list_ = kwarg['original_list'].copy()
    index_ = kwarg['index_pointer']
    rev_length = kwarg['length_reverse']

    big_dict = {i: list_[i] for i in range(len(list_))}

    list_reversed = reverse_list(list_, index_, rev_length)
    indexes_replace = {(i + index_) % len(big_dict.keys()): list_reversed[i] for i in range(len(list_reversed))}
    for k, v in indexes_replace.items():
        big_dict[k] = v

    thing = [big_dict[key] for key in range(len(big_dict))]
    return thing


def run_part_1(kwarg):
    reverse_inputs = kwarg['reverse_inputs']
    numbers_list = kwarg['numbers_list']
    present_index = 0
    # create original list
    # loop over reverse_inputs
    for i in range(len(reverse_inputs)):
        skip_step_index = i
        numbers_list = reset_original({'original_list': numbers_list, 'index_pointer': present_index, 'length_reverse': reverse_inputs[i]})
        # move index one
        present_index = (present_index + skip_step_index + reverse_inputs[i]) % len(numbers_list)
    return numbers_list[0] * numbers_list[1]


def get_reversed_list(kwarg):
    rev_list = kwarg['reverse_inputs']
    pres_index = kwarg['present_index']
    skip_size = kwarg['skip_size']
    the_list = kwarg['numbers_list']
    # loop over reverse_inputs
    for i in range(len(rev_list)):
        skip_step_index = i + skip_size
        the_list = reset_original({'original_list': the_list, 'index_pointer': pres_index, 'length_reverse': rev_list[i]})
        # move index one
        pres_index = (pres_index + skip_step_index + rev_list[i]) % len(the_list)
    return {'numbers_list': the_list, 'skip_size': skip_step_index, 'present_index': pres_index, 'reverse_inputs': rev_list}


def run_part_2(kwarg):
    reverse_inputs = kwarg['reverse_inputs']
    reverse_inputs += [17, 31, 73, 47, 23]
    numbers_list = kwarg['numbers_list']
    present_index = 0
    skip_size = 0
    response = {'numbers_list': numbers_list, 'skip_size': skip_size, 'present_index': present_index, 'reverse_inputs': reverse_inputs}
    for i in range(64):
        response = get_reversed_list(response)
    print(response)


if __name__ == '__main__':
    # Part 1
    # unit test reset_original
    assert reset_original({'original_list': [0, 1, 2, 3, 4], 'index_pointer': 0, 'length_reverse': 3}) == [2, 1, 0, 3, 4]
    assert reset_original({'original_list': [2, 1, 0, 3, 4], 'index_pointer': 3, 'length_reverse': 4}) == [4, 3, 0, 1, 2]
    assert reset_original({'original_list': [4, 3, 0, 1, 2], 'index_pointer': 3, 'length_reverse': 1}) == [4, 3, 0, 1, 2]
    assert reset_original({'original_list': [4, 3, 0, 1, 2], 'index_pointer': 1, 'length_reverse': 5}) == [3, 4, 2, 1, 0]
    # unit test part 1
    input_numbers_list = [i for i in range(5)]
    assert run_part_1({'reverse_inputs': [3, 4, 1, 5], 'numbers_list': input_numbers_list}) == 12
    input_numbers_list = [i for i in range(256)]
    input_list = [147, 37, 249, 1, 31, 2, 226, 0, 161, 71, 254, 243, 183, 255, 30, 70]
    print(run_part_1({'reverse_inputs': input_list, 'numbers_list': input_numbers_list}))

    run_part_2({'reverse_inputs': input_list, 'numbers_list': input_numbers_list})
